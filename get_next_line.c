/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 17:26:13 by lsimonne          #+#    #+#             */
/*   Updated: 2016/02/11 16:18:36 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int		ft_line(char **saved, char **line)
{
	char	*tmp;
	size_t	len;

	len = ft_strchr(*saved, '\n') - *saved;
	tmp = ft_strdup(*saved);
	*line = ft_strsub(*saved, 0, len);
	ft_strdel(saved);
	if (tmp[len + 1] != '\0')
		*saved = ft_strsub(tmp, len + 1, ft_strlen(tmp) - len);
	return (1);
}

static int		ft_end(int ret, char *saved, char **line)
{
	if (ret < 0)
		return (-1);
	if (saved && *saved)
	{
		*line = ft_strdup(saved);
		ft_bzero(saved, ft_strlen(saved));
		return (1);
	}
	return (0);
}

int				get_next_line(int fd, char **line)
{
	static char			*saved = NULL;
	int					ret;
	char				*tmp;
	char				buf[BUFF_SIZE + 1];

	if (saved && ft_strchr(saved, '\n') != NULL)
		return (ft_line(&saved, line));
	if (fd < 0)
		return (-1);
	tmp = ft_strdup("");
	while ((ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = '\0';
		if (saved)
		{
			tmp = ft_strdup(saved);
			ft_strdel(&saved);
		}
		saved = ft_strjoin(tmp, buf);
		ft_strdel(&tmp);
		ft_bzero(buf, BUFF_SIZE);
		if (ft_strchr(saved, '\n') != NULL)
			return (ft_line(&saved, line));
	}
	return (ft_end(ret, saved, line));
}
