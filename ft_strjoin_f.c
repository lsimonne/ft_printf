/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_f.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 13:26:48 by lsimonne          #+#    #+#             */
/*   Updated: 2016/08/29 13:26:50 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin_f(char **s1, char *s2)
{
	char	*res;
	int		i;
	int		j;

	i = -1;
	j = -1;
	if (!s1 || !s2)
		return (NULL);
	res = (char *)malloc((ft_strlen(*s1) + ft_strlen(s2) + 1));
	if (!res)
		return (NULL);
	res = ft_strcpy(res, *s1);
	res = ft_strcat(res, s2);
	ft_strdel(s1);
	return (res);
}
