/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/22 19:28:46 by lsimonne          #+#    #+#             */
/*   Updated: 2015/12/22 20:08:19 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

size_t	ft_word_number(const char *str, char c)
{
	size_t		size;
	const char	*tmp;

	size = 0;
	tmp = str;
	while (*tmp)
	{
		while (*tmp && *tmp == c)
			tmp++;
		if (*tmp && *tmp != c)
		{
			size++;
			tmp++;
		}
		while (*tmp && *tmp != c)
			tmp++;
	}
	return (size);
}

size_t	ft_word_length(char *s, char c)
{
	size_t len;

	len = 0;
	while (s[len] && s[len] != c)
		len++;
	return (len);
}

char	*ft_dup_word(char *start, size_t len)
{
	char	*str;
	size_t	i;

	i = 0;
	if (!(str = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	while (i < len)
	{
		str[i] = start[i];
		i++;
	}
	str[i] = '\0';
	return (str);
}

char	**ft_strsplit(char const *s, char c)
{
	size_t	size;
	size_t	i;
	size_t	len;
	char	**array;
	char	*tmp;

	i = 0;
	if (s == NULL)
		return (NULL);
	size = ft_word_number(s, c);
	tmp = (char *)s;
	if (!(array = (char **)malloc(sizeof(char *) * (size + 1))))
		return (NULL);
	while (i < size)
	{
		while (*tmp && *tmp == c)
			tmp++;
		len = ft_word_length(tmp, c);
		array[i] = ft_dup_word(tmp, c);
		i++;
		tmp = tmp + len;
	}
	array[i] = NULL;
	return (array);
}
