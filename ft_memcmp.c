/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 13:24:22 by lsimonne          #+#    #+#             */
/*   Updated: 2016/08/29 13:24:24 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *dst, const void *src, size_t n)
{
	while (n != 0)
	{
		if (*(unsigned char*)dst != *(unsigned char *)src)
			return (*(unsigned char *)dst - *(unsigned char *)src);
		dst++;
		src++;
		n--;
	}
	return (0);
}
