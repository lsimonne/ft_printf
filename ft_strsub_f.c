/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub_f.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 13:28:49 by lsimonne          #+#    #+#             */
/*   Updated: 2016/08/29 13:28:52 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub_f(char **str, unsigned int start, size_t n)
{
	char	*tmp;

	if (!*str)
		return (NULL);
	tmp = ft_strnew(n);
	if (tmp)
		ft_strncpy(tmp, *str + start, n);
	ft_strdel(str);
	return (tmp);
}
